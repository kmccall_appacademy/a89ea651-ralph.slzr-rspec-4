class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a?(String)
      @entries[entry] = nil
    elsif entry.is_a?(Hash)
      @entries.merge!(entry)
    end
  end

  def include?(entry)
    @entries.has_key?(entry)
  end

  def find(fragment)
    @entries.select do |word, definition|
      word.match(fragment)
    end
  end

  def keywords
    @entries.keys.sort {|a, b| a <=> b}
  end

  def printable
    entries = keywords.map do |keyword|
      %Q{[#{keyword}] "#{@entries[keyword]}"}
  end

  entries.join("\n")
end
end
