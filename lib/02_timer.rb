class Timer

  attr_accessor :seconds

  def initialize(seconds=0)
    @seconds = seconds
  end

  def minutes
    (seconds % 3600) / 60
  end

  def min_sec
    seconds % 60
  end

  def hours
    seconds / 3600
  end

  def padding(num)
    if num > 10
      "#{num}"
    else
      "0#{num}"
    end
  end

  def time_string
      "#{padding(hours)}:#{padding(minutes)}:#{padding(min_sec)}"
  end

end
