class Book
  attr_reader :title

  def title=(title)
    title_arr = title.split(' ').map(&:downcase)
    small_words = ['a', 'of', 'the', 'and', 'an', 'in']
    changed = []
    title_arr.each.with_index do |word, idx|
      if idx == 0
        changed << word.capitalize
      elsif !small_words.include?(word) 
        changed << word.capitalize
      else
        changed << word
      end
    end
    @title = changed.join(' ')
  end
end
